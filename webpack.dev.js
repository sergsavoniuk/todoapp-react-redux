const webpack = require('webpack')
const merge = require('webpack-merge')
const path = require('path')
const common = require('./webpack.common.js')

module.exports = merge(common, {
  entry: [
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    './src/index.jsx',
  ],

  devtool: 'cheap-module-source-map',

  devServer: {
    proxy: {
      '/api': 'http://localhost:8080'
    },
    contentBase: path.resolve(__dirname, 'dist'),
    historyApiFallback: true,
    port: 3000,
    hot: true,
    publicPath: '/',
    compress: true,
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ]
});