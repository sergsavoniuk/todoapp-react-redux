const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: {
    app: './src/index.jsx',
    vendors: [
      'react',
      'react-dom',
      'redux',
      'react-redux',
      'redux-thunk',
      'react-router',
    ],
    polyfill: 'babel-polyfill',
  },

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/bundle.js',
  },

  resolve: {
    modules: ["node_modules"],
    extensions: ['*', '.js', '.jsx', '.json', '.css', 'html'],
  },

  module: {
    loaders: [
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'eslint-loader']
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: {
                  autoprefixer: {
                    add: true,
                    remove: true,
                    browsers: ['last 2 versions'],
                  },
                  discardComments: {
                    removeAll: true,
                  },
                  discardUnused: false,
                  mergeIdents: false,
                  reduceIdents: false,
                  safe: true,
                },
              },
            },
          ]
        })
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        exclude: /node_modules/,
        use: ['url-loader?limit=10000&mimetype=image/svg+xml']
      },
      {
        test: /\.gif/,
        exclude: /node_modules/,
        use: ['url-loader?limit=10000&mimetype=image/gif']
      },
      {
        test: /\.jpg/,
        exclude: /node_modules/,
        use: ['url-loader?limit=10000&mimetype=image/jpg']
      },
      {
        test: /\.png/,
        exclude: /node_modules/,
        use: ['url-loader?limit=10000&mimetype=image/png']
      },
    ]
  },

  plugins: [
    new ExtractTextPlugin({
      filename: 'styles/bundle.css',
      allChunks: true,
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/index.html'),
      filename: 'index.html',
    }),
    new webpack.NoEmitOnErrorsPlugin(),
  ]
}