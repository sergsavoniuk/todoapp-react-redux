import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import { MemoryRouter, Switch } from "react-router-dom"

import Main from './layout/Main'
import Root from './Root'

let store = configureStore()

ReactDOM.render((
  <Provider store={store}>
    <MemoryRouter>
      <div>
        <Main />
        <Root />
      </div>
    </MemoryRouter>
  </Provider>
), document.getElementById('root'))