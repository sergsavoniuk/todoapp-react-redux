import React from 'react'
import { connect } from 'react-redux'
import { addTodo } from '../../actions/todos'
import './addTodo.css'

class AddTodo extends React.Component {

  constructor(props) {
    super(props);
  }

  onSubmit = (event) => {
    event.preventDefault();
    this.props.addTodo(this.input.value);
    this.input.value = '';
  }

  render() {
    return (
      <form onSubmit={this.onSubmit} className="addTodo">
        <input
          type="text"
          placeholder="Type here ..."
          ref={input => this.input = input}
        />
        <input type="submit" />
      </form>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addTodo: text => dispatch(addTodo(text))
  }
}

export default connect(null, mapDispatchToProps)(AddTodo)