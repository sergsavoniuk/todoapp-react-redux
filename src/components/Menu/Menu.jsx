import React from 'react'
import { Link } from 'react-router-dom'

class Menu extends React.Component {
  render() {
    return (
      <div>
        <ul>
          <li><Link to='/home'>Home</Link></li>
          <li><Link to='/todos'>TodoList</Link></li>
        </ul>
      </div>
    )
  }
}

export default Menu