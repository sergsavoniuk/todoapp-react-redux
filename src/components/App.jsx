import React from 'react'
import AddTodo from './AddTodo/AddTodo'
import TodoList from './TodoList/TodoList'
import './app.css'

const App = () => (
  <div className='container'>
    <AddTodo />
    <TodoList />
  </div>
)

export default App