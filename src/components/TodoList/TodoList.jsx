import React from 'react'
import { connect } from 'react-redux'
import Todo from './Todo'
import { toggleTodo } from '../../actions/todos'
import './todolist.css'

class TodoList extends React.Component {

  constructor(props) {
    super(props)
  }

  onClick = (todoId) => {
    this.props.onTodoClick(todoId);
  }

  render() {
    const { todos, onTodoClick } = this.props
    return (
      <ul className="todo-list">
        {
          todos && todos.map(todo => {
            return (
              <Todo
                key={todo.id}
                onClick={() => this.onClick(todo.id)}
                {...todo}
              />
            )
          })
        }
      </ul>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    todos: state.todos
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onTodoClick: id => dispatch(toggleTodo(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)