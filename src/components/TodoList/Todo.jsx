import React from 'react'
import './todolist.css'

const Todo = ({ text, completed, onClick }) => {
  return (
    <li className={completed ? 'cross-out' : ''}>
      <div>
        <input
          type="checkbox"
          onChange={onClick}
        />
      </div>
      <span>
        {text}
      </span>
    </li>
  )
}

export default Todo