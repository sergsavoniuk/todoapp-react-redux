import { ADD_TODO, TOGGLE_TODO } from '../constants/actionTypes'

const initialState = [
  {
    text: 'Use Redux',
    id: 0,
    completed: false,
  },
]

const todoApp = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return [
        ...state,
        {
          id: state.reduce((maxId, todo) => Math.max(todo.id, maxId), -1) + 1,
          text: action.text,
          completed: false,
        },
      ]
    case TOGGLE_TODO:
      return state.map((todo) => {
        if (todo.id === action.id) {
          return { ...todo, completed: !todo.completed }
        }
        return todo
      })
    default:
      return state
  }
}

export default todoApp