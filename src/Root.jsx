import { Router, Route, IndexRoute, browserHistory, MemoryRouter, Switch } from "react-router-dom"
import App from './components/App'
import Home from './components/Home/Home'
import React from "react"

export const Path = {
  root: '/',
  home: '/home',
  todos: '/todos',
}

export default () =>
  <Switch>
    <Route path={Path.todos} component={App} />
    <Route path={Path.home} component={Home} />
    <Route path="*" component={Home} />
  </Switch>